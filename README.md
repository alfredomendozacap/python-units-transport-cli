# Units Transports CLI

A CLI app to perform CRUD operations on transport units. 🚗🚙🚚🚌

## Table Diagram

| Attributes | Data Types |
|------------:|------------|
| brand | varchar |
| model | varchar |
| car_registration | varchar |
| type_of_use | varchar |
| people_capacity | int |
| loading_capacity | int |
| purchased_at | date |
| last_maintenance | date |
___

## How to Clone this repo

1. Clone this project.
2. Create a virtual env ```py -m venv .venv```.
3. Install Dependencies ```pip install -r requirements.txt```.


## Database Setup

1. Duplicate the file ```config.example.ini``` , rename it to ```config.ini``` and change the parameters.
2. Duplicate the file ```table.example.sql```, rename it to ```table.sql``` and change the table definition.
3. Run ```py src/create_tables.py``` to migrate the table.

## Usage

1. See the command options by running ```py src/main.py --help```.

### Available Commands

~~~bash
create   Create a New Transport
getall   Get All the Transports
getbyid  Get a Transport by ID
~~~
