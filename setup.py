from setuptools import setup

with open('README.md', 'r') as f:
    readme = f.read()
    f.close()

setup(
    name='TransportsUnits',
    version='0.1.0',
    description='TransportsUnits Base project',
    long_description=readme,
    author='Alfredo Mendoza',
    author_email='alfredo.mendoza@agavesoft.com.mx',
    url='https://github.com/alfredomendozacap/transports-units',
)
