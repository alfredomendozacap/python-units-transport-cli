import psycopg2
from config import config

class DB():
    conn = None
    def getConnection(self):
        try:
            # read connection parameters
            params = config()

            # connect to the PostgreSQL server
            self.conn = psycopg2.connect(**params)
            return self.conn
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)


    def closeConnection(self):
        try:
            self.conn.close()
        except (Exception, psycopg2.Error) as error:
            return ("Error PostgreSQL", error)
