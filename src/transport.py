import db
from psycopg2 import DatabaseError, InternalError

class Transport():
    id = 0
    brand = ''
    model = ''
    carRegistration = ''
    typeOfUse = ''
    peopleCapacity = ''
    carryingCapacity = ''
    purchasedAt = ''
    lastMaintenance = ''


    def create(self):
        q = """INSERT INTO transport_units (brand, model, car_registration, type_of_use, people_capacity, carrying_capacity, purchased_at, last_maintenance)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s) RETURNING id"""
        data = (
            self.brand,
            self.model,
            self.carRegistration,
            self.typeOfUse,
            self.peopleCapacity,
            self.carryingCapacity,
            self.purchasedAt,
            self.lastMaintenance
        )

        try:
            dbI = db.DB()
            conn = dbI.getConnection()

            cur = conn.cursor()
            cur.execute(q, data)

            lastId = cur.fetchone()[0]

            rowCount = cur.rowcount

            conn.commit()
            cur.close()

            if rowCount != 1:
                raise Exception("Transport cannot be created")
                return None

            print ("Record inserted successfully with id:", lastId)
            return (lastId,) + data
        except (Exception, DatabaseError) as error:
            print('Error: ', error)
        finally:
            if conn is not None:
                dbI.closeConnection()

    def update(self):
        q = """UPDATE transport_units SET brand = %s, model = %s, car_registration = %s, type_of_use = %s, people_capacity = %s, carrying_capacity = %s, purchased_at = %s, last_maintenance = %s WHERE id = %s"""
        data = (
            self.brand,
            self.model,
            self.carRegistration,
            self.typeOfUse,
            self.peopleCapacity,
            self.carryingCapacity,
            self.purchasedAt,
            self.lastMaintenance
        )

        try:
            dbI = db.DB()
            conn = dbI.getConnection()

            cur = conn.cursor()
            cur.execute(q, data+(self.id,))

            rowCount = cur.rowcount
            conn.commit()
            cur.close()

            if rowCount != 1:
                raise Exception("Transport with id {} cannot be updated".format(self.id))
                return None

            print ("Record updated successfully with id:", self.id)
            return (self.id,) + data
        except (Exception, DatabaseError) as error:
            print('Error: ', error)
        finally:
            if conn is not None:
                dbI.closeConnection()

    def getAll(self):
        q = """SELECT
                id, brand, model, car_registration, type_of_use, people_capacity,
                carrying_capacity, purchased_at, last_maintenance
            FROM transport_units"""

        try:
            dbI = db.DB()
            conn = dbI.getConnection()

            cur = conn.cursor()
            cur.execute(q)

            rows = cur.fetchall()
            rowCount = cur.rowcount
            print('There are {} transports'.format(rowCount))
            cur.close()

            if rowCount == 0:
                raise Exception("No Data Found")
                return None

            return rows
            print ("Record inserted successfully with id:", lastId)
        except (Exception, DatabaseError) as error:
            print('Error: ', error)
        finally:
            if conn is not None:
                dbI.closeConnection()

    def getByID(self):
        q = """SELECT
                id, brand, model, car_registration, type_of_use, people_capacity,
                carrying_capacity, purchased_at, last_maintenance
            FROM transport_units WHERE id = %s"""

        try:
            dbI = db.DB()
            conn = dbI.getConnection()

            cur = conn.cursor()
            cur.execute(q, (self.id,))

            row = cur.fetchone()
            rowCount = cur.rowcount
            cur.close()

            if rowCount != 1:
                raise Exception("No Data Found")
                return None

            return row
        except (Exception, DatabaseError) as error:
            print('Error: ', error)
        finally:
            if conn is not None:
                dbI.closeConnection()

    def delete(self):
        q = """DELETE FROM transport_units WHERE id = %s"""

        try:
            dbI = db.DB()
            conn = dbI.getConnection()

            cur = conn.cursor()
            cur.execute(q, (self.id,))

            rowCount = cur.rowcount

            conn.commit()
            cur.close()

            if rowCount != 1:
                raise Exception("Transport with id {} cannot be deleted".format(self.id))
                return None

            print("Transport with id {} deleted succesfully".format(self.id))
            return None
        except (Exception, DatabaseError) as error:
            print('Error: ', error)
        finally:
            if conn is not None:
                dbI.closeConnection()
