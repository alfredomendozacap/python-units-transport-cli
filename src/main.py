import click
from transport import Transport
from datetime import date

@click.group()
def crud():
    pass

@crud.command()
def getAll():
    ''' GetAll Transports '''

    transport = Transport()
    transports = transport.getAll()
    if transport is not None:
        for t in transports:
            printTransport(t)

@crud.command()
def getByID():
    ''' Get a Transport '''

    transport = Transport()
    transport.id = click.prompt('Enter the transport ID', type=int)
    t = transport.getByID()
    if t is not None:
        printTransport(t)


@crud.command()
def create():
    ''' Create Transport '''

    newTransport = Transport()

    newTransport.brand = click.prompt('Enter the transport brand', type=str, default=None)
    newTransport.model = click.prompt('Enter the transport model', type=str, default=None)
    newTransport.carRegistration = click.prompt('Enter the transport registration', type=str, default=None)
    newTransport.typeOfUse = click.prompt('Enter the transport type of use', type=str, default=None)
    newTransport.peopleCapacity = click.prompt('Enter the people Capacity', type=int ,default=None)
    newTransport.carryingCapacity = click.prompt('Enter the carrying Capacity', type=int ,default=None)
    newTransport.purchasedAt = click.prompt('Enter the purchase date', type=click.DateTime(formats=['%Y-%m-%d']), default=None)
    newTransport.lastMaintenance = click.prompt('Enter the last maintenance date', type=click.DateTime(formats=['%Y-%m-%d']), default=None)

    t = newTransport.create()
    if t is not None:
        printTransport(t)

@crud.command()
def update():
    ''' Update Transport '''

    transport = Transport()

    transport.brand = click.prompt('Enter the new transport brand', type=str, default=None)
    transport.model = click.prompt('Enter the new transport model', type=str, default=None)
    transport.carRegistration = click.prompt('Enter the new transport registration', type=str, default=None)
    transport.typeOfUse = click.prompt('Enter the new transport type of use', type=str, default=None)
    transport.peopleCapacity = click.prompt('Enter the new people Capacity', type=int ,default=None)
    transport.carryingCapacity = click.prompt('Enter the new carrying Capacity', type=int ,default=None)
    transport.purchasedAt = click.prompt('Enter the new purchase date', type=click.DateTime(formats=['%Y-%m-%d']), default=None)
    transport.lastMaintenance = click.prompt('Enter the new last maintenance date', type=click.DateTime(formats=['%Y-%m-%d']), default=None)
    transport.id = click.prompt('Enter the transport ID you want to update', type=int ,default=None)

    t = transport.update()
    if t is not None:
        printTransport(t)

@crud.command()
def delete():
    ''' Delete Transport '''

    transport = Transport()

    transport.id = click.prompt('Enter the transport ID you want to delete', type=int ,default=None)

    transport.delete()

def printTransport(t=()):
        click.echo("""\
            ID: {}
            Brand: {}
            Model: {}
            CarRegistration: {}
            TypeOfUse: {}
            PeopleCapacity: {}
            CarryingCapacity: {}
            PurchasedAt: {}
            LastMaintenance: {}\n""".format(t[0],t[1],t[2],t[3],t[4],t[5],t[6],t[7],t[8]))

if __name__ == "__main__":
    crud()
