CREATE TABLE transport_units (
    id SERIAL PRIMARY KEY,
    brand VARCHAR(255) NOT NULL,
    model VARCHAR(255) NOT NULL,
    car_registration VARCHAR(255) NOT NULL,
    type_of_use VARCHAR(255) NOT NULL,
    people_capacity INT NOT NULL,
    carrying_capacity INT NOT NULL,
    purchased_at DATE NOT NULL,
    last_maintenance DATE NOT NULL,

    CONSTRAINT transport_units_car_registration_uq UNIQUE (car_registration)
)
